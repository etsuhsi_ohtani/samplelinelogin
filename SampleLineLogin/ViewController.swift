//
//  ViewController.swift
//  SampleLineLogin
//
//  Created by Etsushi Otani on 2021/08/20.
//

import UIKit
import LineSDK

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        checkToken()
    }

    @IBAction func buttonTapped(_ sender: Any) {
        lineLogin()
    }
    
    func lineLogin() {
        let permissions: Set<LoginPermission> = [.profile]
        //let permissions: Set<LoginPermission> = [.profile, .openID, .email]
        
        LoginManager.shared.login(permissions: permissions, in: self) { result in
            switch result {
            case .success(let result):
                let accessToken = result.accessToken.value
                let prifile = result.userProfile
                // let idToken = result.accessToken.IDToken
                // let email = result.accessToken.IDToken?.payload.email
                
            case .failure(let error):
            if error.isUserCancelled {
                    // User cancelled the login process himself/herself.
                    
                } else if error.isPermissionError {
                    // Equivalent to checking .responseFailed.invalidHTTPStatusAPIError
                    // with code 403. Should login again.
                    
                } else if error.isURLSessionTimeOut {
                    // Underlying request timeout in URL session. Should try again later.
                    
                } else if error.isRefreshTokenError {
                    // User is accessing a public API with expired token, LINE SDK tried to
                    // refresh the access token automatically, but failed (due to refresh token)
                    // also expired. Should login again.
                    
                } else {
                    // Any other errors.
                    print("\(error)")
                }
            }
        }
    }
    
    func getToken() {
        if let token = AccessTokenStore.shared.current {
            print(token.value)
        }
    }
    
    func checkToken() {
        API.Auth.verifyAccessToken { result in
            switch result {
            case .success(let value):
                print(value.channelID) // Bound channel ID of the token.
                print(value.permissions) // The permissions of this token.
                print(value.expiresIn) // How long it is before the token expires.
            case .failure(let error):
                print(error)
            }
        }
    }
    
}

